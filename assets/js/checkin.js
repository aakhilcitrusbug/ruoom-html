$(document).ready(function () {
  $('select').select2({
    minimumResultsForSearch: -1,
    placeholder: "Select"
  });
});


$('.input-date').click(function () {
  if (this.checked) {
    $(".input-date-read").attr("readonly", false);

  }
  else {
    $(".input-date-read").attr("readonly", true);
  
  }

});